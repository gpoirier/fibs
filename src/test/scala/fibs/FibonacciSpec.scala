package fibs

//import System.{ currentTimeMillis => now }
import System.{ nanoTime => now }
import org.scalatest._
import org.scalatest.matchers.ShouldMatchers

class FibonacciSpec extends FlatSpec with ShouldMatchers {
  "Recursive fibbonacci" should "work" in {
    ((0 to 10) map Fibonacci.recursive).toList should be === List(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55)
  }

  "All fibbonacci algo" should "work the same" in {
    val range = 0 to 40
    def test(name: String, f: Int => Int): List[Int] = {
      val before = now
      val list = (range map f).toList     
      val after = now
      println(s"$name: ${after - before} ns")
      list
    }
    
    val expected = test("Recursive", Fibonacci.recursive)
    for ((algo, index) <- Fibonacci.streamAlgos.zip(Stream.from(0))) {
      val actual = test(s"Stream #${index + 1}", algo)
      actual should be === expected
    }
  }
}
