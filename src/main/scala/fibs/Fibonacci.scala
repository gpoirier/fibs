package fibs

object Fibonacci {
  def recursive(n: Int): Int = 
    if (n == 0) 0
    else if (n == 1) 1
    else (recursive(n - 2) + recursive(n - 1))

  val streamAlgos = List(
    (n: Int) => {
      lazy val fibs: Stream[Int] = 0 #:: 1 #:: Stream.from(2).map(i => fibs(i - 2) + fibs(i - 1))
      fibs(n)
    },
    (n: Int) => {
      lazy val fibs: Stream[Int] = 0 #:: 1 #:: fibs.zip(fibs.tail).map { case (a, b) => a + b }
      fibs(n)
    },
    (n: Int) => {
      def tail(a: Int, b: Int): Stream[Int] = a #:: tail(b, a + b)
      tail(0, 1)(n)
    },
    (n: Int) => {
      lazy val fibs: Stream[Int] = 0 #:: fibs.scanLeft(1)(_ + _)
      fibs(n)
    }
  )
}
